var myApp = angular.module('myApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'LocalStorageModule'])
    .config(function ($locationProvider, $routeProvider, localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('storage');

        $routeProvider
            .when('/event', {
                templateUrl: 'show.event.template.html',
                controller: 'EventCtrl'
            })
            .otherwise({redirectTo: '/event'});
    });
myApp.controller('EventCtrl', function ($scope,localStorageService) {

    var index = 0;
    $scope.newEvent = {
        id: "",
        title: "",
        content: "",
        date: new Date()
    };

    $scope.eventNumber = 4;
    $scope.eventTemplate = true;
    $scope.addEventTemplate = false;
    $scope.aboutTemplate = false;
    if (localStorageService.get('event')) {
        $scope.events = localStorageService.get('event');
    }
    else {

        $scope.events = [
            {
                id: 0,
                title: 'Eveniment1',
                content: 'Continut...',
                date: new Date("2015-03-25")
            },
            {
                id: 1,
                title: 'Eveniment2',
                content: 'Continut...',
                date: new Date("2015-03-25")
            },
            {
                id: 2,
                title: 'Eveniment3',
                content: 'Continut...',
                date: new Date("2015-03-25")
            },
            {
                id: 3,
                title: 'Eveniment4',
                content: 'Continut...',
                date: new Date("2015-03-25")
            }

        ];
    }
    localStorageService.set('event', $scope.events);

    $scope.event = $scope.events[index];
    $scope.anotherEvent = function (event) {
        if (event.key == "ArrowLeft") {
            index++;
            if (index >= $scope.events.length) {
                index = 0;
            }
            $scope.event = $scope.events[index];

            $('#top').addClass('animated slideInLeft')
                .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $(this).removeClass('animated slideInLeft');
                });
            $scope.eventTemplate = true;
            $scope.addEventTemplate = false;
            $scope.aboutTemplate = false;
        }
        if (event.key == "ArrowRight") {
            index--;
            if (index < 0) {
                index = $scope.events.length - 1;
            }
            $scope.event = $scope.events[index];
            $('#top').addClass('animated slideInRight')
                .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $(this).removeClass('animated slideInRight');
                });
            $scope.eventTemplate = true;
            $scope.addEventTemplate = false;
            $scope.aboutTemplate = false;
        }
        if (event.key == "ArrowDown") {
            $('#top').addClass('animated slideInDown')
                .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $(this).removeClass('animated slideInDown');
                });
            $scope.eventTemplate = false;
            $scope.addEventTemplate = true;
            $scope.aboutTemplate = false;
        }
        if (event.key == "ArrowUp") {
            $('#top').addClass('animated slideInUp')
                .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $(this).removeClass('animated slideInUp');
                });
            $scope.eventTemplate = false;
            $scope.addEventTemplate = false;
            $scope.aboutTemplate = true;
        }
    };


    $scope.saveEvent = function (event) {

        $scope.newEvent = {
            id: "",
            title: "",
            content: "",
            date: null
        };
        if (event.id == "") {
            $scope.events.push({
                id: $scope.eventNumber,
                title: event.title,
                content: event.content,
                date: event.date
            });
            $scope.eventNumber++;
        }
        else {
            for (var i = 0; i < $scope.events.length; i++) {
                if ($scope.events[i].id == event.id) {
                    $scope.events[i].title = event.title;
                    $scope.events[i].content = event.content;
                    $scope.events[i].date = event.date;
                    break;
                }
            }
        }
        sortEvent();
        localStorageService.set('event', $scope.events);
        $scope.anotherEvent({key: "ArrowRight"});
    };

    $scope.removeEvent = function (id) {

        for (var i = 0; i < $scope.events.length; i++) {
            if ($scope.events[i].id == id) {
                $scope.events.splice(i, 1);
                break;
            }
        }
        sortEvent();
        localStorageService.set('event', $scope.events);
        $scope.anotherEvent({key: "ArrowLeft"});
    };

    $scope.editEvent = function (event) {
        $scope.newEvent = event;
        sortEvent();
        $scope.anotherEvent({key: "ArrowDown"});
    };


    function sortEvent() {

        for (var i = 0; i < $scope.events.length; i++) {
            for (var j = i + 1; j < $scope.events.length; j++) {
                if ($scope.events[i].date > $scope.events[j].date) {
                    var temp = $scope.events[i];
                    $scope.events[i] = $scope.events[j];
                    $scope.events[j] = temp;
                }
            }
        }
    }


    $scope.clear = function () {
        $scope.newEvent.date = null;
    };

    $scope.dateOptions = {
        minDate: new Date()
    };

    $scope.datepickerOpen = function () {
        $scope.datepicker.opened = true;
    };

    $scope.datepicker = {opened: false};

});
